*** Settings ***
Library     Collections
Library     RequestsLibrary

*** Variable ***
${URL}             https://superheroapi.com/api
${ACESS_TOKEN}      1499497106915729

*** Keywords ***
### DADO 
Que esteja conectado na SuperHeroAPI
    Conecta ao WebService

### QUANDO
Requisitar o histórico do super herói "${SUPER_HEROI_CONSULTADO}"
    Realiza requisição do histórico do super herói     ${SUPER_HEROI_CONSULTADO}

Requisitar as estatísticas de poder dos super heróis "${PRIMEIRO_HEROI}" e "${SEGUNDO_HEROI}"
    Realiza requisição das estatísticas dos super heróis   ${PRIMEIRO_HEROI}      ${SEGUNDO_HEROI}

### ENTAO
Deve ser retornado que a sua inteligência é superior a "${INTELIGENCIA}"
    Consulta inteligencia do herói    ${INTELIGENCIA}

Deve ser retornado que o mais inteligente é o herói "${HEROI_MAIS_INTELIGENTE}"
    Verifica herói mais inteligente     ${HEROI_MAIS_INTELIGENTE}

## E
Deve ser retornado que o seu verdadeiro nome é "${NOME_HEROI}"
    Confere nome do herói   ${NOMEHEROI}

Deve ser retornado que é afiliado do grupo "${GRUPO_AFILIADO}"
    Confere grupo afiliado  ${GRUPO_AFILIADO}

Deve ser retornado que o mais rápido é o herói "${HEROI_MAIS_RAPIDO}"
    Confere herói mais rapido   ${HEROI_MAIS_RAPIDO}

deve ser retornado que o mais forte é o herói "${HEROI_MAIS_FORTE}"
    Confere herói mais forte     ${HEROI_MAIS_FORTE}

### PASSOS
Conecta ao WebService
    Create Session      consultaHeroi       ${URL}        disable_warnings=1

Realiza requisição do histórico do super herói
    [Arguments]          ${SUPERHEROI}
    ${RESPOSTA}=         Get Request      consultaHeroi   /${ACESS_TOKEN}/search/${SUPERHEROI}
    Set Test Variable    ${RESPOSTA}

Consulta inteligencia do herói
    [Arguments]          ${INTELIGENCIA}
    Deve ser maior que   ${RESPOSTA.json()['results'][0]['powerstats']['intelligence']}     ${INTELIGENCIA}

Confere nome do herói
    [Arguments]         ${NOME_HEROI}
    Should Be equal     ${RESPOSTA.json()['results'][0]['biography']['full-name']}        ${NOME_HEROI}

Confere grupo afiliado
    [Arguments]         ${GRUPO_AFILIADO}     
    Should Contain      ${RESPOSTA.json()['results'][0]['connections']['group-affiliation']}  ${GRUPO_AFILIADO}

Realiza requisição das estatísticas dos super heróis
    [Arguments]                  ${PRIMEIRO_HEROI}       ${SEGUNDO_HEROI}
    ${DADOS_PRIMEIRO_HEROI}=     Get Request     consultaHeroi   ${ACESS_TOKEN}/search/${PRIMEIRO_HEROI}
    ${DADOS_SEGUNDO_HEROI}=      Get Request     consultaHeroi   ${ACESS_TOKEN}/search/${SEGUNDO_HEROI}
    Set Test Variable            ${DADOS_PRIMEIRO_HEROI} 
    Set Test Variable            ${DADOS_SEGUNDO_HEROI}

Verifica herói mais inteligente 
    [Arguments]                     ${HEROI_MAIS_INTELIGENTE}
    Deve ser maior que              ${DADOS_SEGUNDO_HEROI.json()['results'][0]['powerstats']['intelligence']}      ${DADOS_PRIMEIRO_HEROI.json()['results'][1]['powerstats']['intelligence']}
    Log                             ${DADOS_SEGUNDO_HEROI.json()['results'][0]['powerstats']['intelligence']} 
    Should Be Equal As Strings      ${DADOS_SEGUNDO_HEROI.json()['results'][0]['name']}                            ${HEROI_MAIS_INTELIGENTE}
 
Confere herói mais rapido
    [Arguments]                     ${HEROI_MAIS_RAPIDO}
    Deve ser maior que              ${DADOS_PRIMEIRO_HEROI.json()['results'][1]['powerstats']['speed']}             ${DADOS_SEGUNDO_HEROI.json()['results'][0]['powerstats']['speed']}
    Should Be Equal As Strings      ${DADOS_PRIMEIRO_HEROI.json()['results'][1]['name']}                            ${HEROI_MAIS_RAPIDO}


Confere herói mais forte
    [Arguments]                     ${HEROI_MAIS_FORTE}
    Deve ser maior que              ${DADOS_SEGUNDO_HEROI.json()['results'][0]['powerstats']['strength']}          ${DADOS_PRIMEIRO_HEROI.json()['results'][1]['powerstats']['strength']}
    Should bE Equal As Strings      ${DADOS_SEGUNDO_HEROI.json()['results'][0]['name']}                            ${HEROI_MAIS_FORTE}


### Validação de valores
Deve ser maior que
    [Arguments]     ${value_1}   ${value_2}
    Should Be True  ${value_1} > ${value_2}  msg=O Valor ${value_1} não é maior que ${value_2}