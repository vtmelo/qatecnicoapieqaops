# Base image
FROM fedora:29
FROM python

# Define volume suites
VOLUME opt/robot/suites/
VOLUME opt/robot/results/

# Copy files
COPY /suites/ opt/robot/suites/

# Install robotframework and requests library
RUN pip install robotframework \
    robotframework-requests \
    pip install --upgrade pip

# Init case tests
RUN robot -d opt/robot/results/ opt/robot/suites/TestCasesAPIRequests.robot